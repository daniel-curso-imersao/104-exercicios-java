package b_intermediarios;

import java.util.*;
import java.util.Scanner;

/**
 * Dado um numero inteiro positivo, encontre a menor soma de dois números cujo
 * produto seja igual ao número
 * 
 * Ex: se n = 10, então resultado = 7, pois 5 * 2 é o menor produto inteiro
 * positivo que resulta 10.
 * 
 * Testes:
 * 
 */
public class ComplexidadeInteiros {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite um numero:");
		int numero = scanner.nextInt();
		boolean encontrou = false;
		int multiplicador1 = 0;
		int multiplicador2 = 0;
		
		
		for (int i = 1; i <= numero; i++) {
			for (int j = 1; j <= numero ; j++) {
				int produto = (i * j);
				if (produto == numero) {
					int valorAtual = multiplicador1 + multiplicador2;
					int valorFor = i + j;
					if (valorAtual > valorFor || valorAtual == 0) {
						multiplicador1 = j;
						multiplicador2 = i;
					}					
				}
			}			
		}
		
		System.out.println("Os menores multiplicadores são: " + multiplicador1 + " e " + multiplicador2);		
	}

}

package b_intermediarios;

import java.io.IOException;
import java.nio.file.*;
import java.text.MessageFormat;
import java.util.*;
import util.Util;

/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada iteração, o sistema deve pedir uma única tarefa e exibir a
 * possibilidade de encerrar o programa.
 * 
 * Quando o usuário encerrar o programa, deve-se gerar o arquivo txt com as
 * tarefas que foram inseridas.
 *
 */
public class ListaTarefas {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String comando = "";
		List<String> listaTarefas = new ArrayList<>();
		Path pathEscrita = Paths.get("src/tarefas.txt");		
		
		System.out.println("Digite uma tarefa para adicionar na lista ou 'q' para sair: ");
		comando = scanner.nextLine();
		while (!comando.equals("q")) {
			listaTarefas.add(comando);
			System.out.println("Digite uma tarefa para adicionar na lista ou 'q' para sair: ");
			comando = scanner.nextLine();
		}
		
		StringBuilder sb = new StringBuilder();
		listaTarefas.forEach(tarefa -> {
			if (!Util.isUltimoDaLista(listaTarefas, tarefa)) {
				sb.append(tarefa + Util.separador);
			} else {
				sb.append(tarefa);
			}				
		});
		
		try {				
			Files.deleteIfExists(pathEscrita);
			Files.createFile(pathEscrita);			
			Files.write(pathEscrita, sb.toString().getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
				
		System.out.println(MessageFormat.format("Arquivo gravado com sucesso em: {0}", pathEscrita));
	}
}

package b_intermediarios;

/**
 * Crie um programa que simula o funcionamento de um caça níquel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte critério:
 * 
 * Caso hajam três valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam três valores iguais: 1000 pontos
 * Caso hajam três valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
	public static void main(String args[]) {
		String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
					
		String valor1 = sortear(valores);
		String valor2 = sortear(valores);
		String valor3 = sortear(valores);
		int pontuacao = 0;
		
		// 3 valores "7"
		if ((valor1.equals(valor2)) && (valor2.equals(valor3)) && 
					(valor1.equals("7"))) {
			pontuacao = pontuacao + 5000;
		}		
		// 3 valores iguais
		else if ((valor1.equals(valor2)) && (valor2.equals(valor3))) {
			pontuacao = pontuacao + 1000;
		} 
		// 2 valores iguais
		else if ((valor1.equals(valor2)) || (valor2.equals(valor3)) || (valor1.equals(valor3))) {
			pontuacao = pontuacao + 100;
		}	
		// todo mundo diferente
		else if ((!valor1.equals(valor2)) && (!valor2.equals(valor3)) && (!valor1.equals(valor3))) {
			pontuacao = pontuacao + 0;				
		}
							
		System.out.println("A pontuação foi de " + pontuacao);
	}
	
	public static String sortear(String[] valores) {
		int indice = (int) Math.floor(Math.random() * valores.length);
		return valores[indice];
	}
}

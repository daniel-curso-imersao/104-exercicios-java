package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.*;

import javax.imageio.stream.FileCacheImageInputStream;

import util.Util;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e filtre
 * aqueles que possuem o saldo superior à 7000. Desses clientes, criar outro
 * arquivo de texto que possua todos os clientes no seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails {
	public static void main(String[] args) {
		Path pathLeitura = Paths.get("src/contas.csv");
		Path pathEscrita = Paths.get("src/contas7000.csv");
		List<String> linhas;
		StringBuilder sb = new StringBuilder("nome_concatenado" + Util.separador);
		try {
			linhas = Files.readAllLines(pathLeitura);

			for (String linha : linhas) {
				if (!Util.isPrimeiroItem(linhas, linha)) {
					String[] array = linha.split(",");
					int saldo = Integer.parseInt(array[4]);
					if (saldo > 7000) {
						String primeiroNome = array[1];
						String ultimoNome = array[2];
						String email = array[3];
						String linhaGravar = MessageFormat.format("{0} {1}<{2}>,{3}", primeiroNome, ultimoNome, email,
								Util.separador);
						sb.append(linhaGravar);
					}
				}
			}
			
			// Remover a última vírgula
			sb.deleteCharAt(sb.lastIndexOf(","));

			Util.inicializarArquivoExercicio(pathEscrita, sb.toString().getBytes());

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}

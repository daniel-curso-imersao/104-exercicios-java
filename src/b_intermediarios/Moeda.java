package b_intermediarios;

import java.text.MessageFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

import util.Util;

/**
 * Crie um programa que receba um valor decimal e o converta para o formato
 * moeda. Deve-se fazer as devidas validações para que uma entrada incorreta de
 * dados não interrompa a execução do programa.
 * 
 */
public class Moeda {
	public static void main(String[] args) {
		boolean numeroValido = false;
		String valor = "";
		double moeda = 0;
		
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("Inserir um valor decimal: ");		
			while (!numeroValido) {
				try {
					valor = scanner.nextLine();
					moeda = Double.parseDouble(valor);
					numeroValido = true;				
				} catch (NumberFormatException e) {
					System.out.println(MessageFormat
							.format("O valor inserido: {0} é inválido. Favor inserir um valor decimal.", valor));
				}
			}
		}
									
		String reais = "R$" + Util.formatarReais(moeda);
		System.out.println("O valor em reais é: " + reais);
	}
}

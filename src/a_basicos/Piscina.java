package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe as três dimensões de uma piscina
 * (largura, comprimento, profundidade) e retorne a sua capacidade
 * em litros.
 */
public class Piscina {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double largura = scanner.nextDouble();
		double comprimento = scanner.nextDouble();
		double profundidade = scanner.nextDouble();
		
		double litros = (largura * comprimento * profundidade) * 1000;
		
		System.out.println("A capacidade em litros é " + litros);
	}
}

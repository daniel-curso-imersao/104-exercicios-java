package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe uma palavra do terminal e determina
 * se ela é um palíndromo.
 * 
 * Exs: 
 * 
 * input: ovo
 * output: A palavra ovo é um palíndromo
 * 
 * input: jose
 * output: A palavra jose não é um palíndromo 
 */
public class Palindromo {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		String palavra = scanner.nextLine();
		char[] invertido = new char[palavra.length()];
		int j = 0;
		
		for (int i = palavra.length() - 1; i >= 0; i--) {
			invertido[j] = palavra.charAt(i);
			j++;							
		}

		String invertidoComparar = String.valueOf(invertido);
		
		if (palavra.equals(invertidoComparar)) {
			System.out.println("A palavra " + palavra + " é um palíndromo.");
		} else {
			System.out.println("A palavra " + palavra + " não é um palíndromo.");
		}				
	}
}

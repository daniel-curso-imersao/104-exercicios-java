package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe um número do usuário
 * e imprime a fatorial desse mesmo número.
 * 
 */
public class Fatorial {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int numero = scanner.nextInt();
		int cont = numero;
		while (cont > 1) {
			cont = cont -1;
			numero = numero * cont;
		}
		System.out.println("Total: " + numero);
	}
}

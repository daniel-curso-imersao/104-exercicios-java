package a_basicos;

import java.util.*;

/**
 * Crie um programa que imprima todos os números
 * primos de 1 à 100
 */
public class Primo {
	public static void main(String[] args) {
		List<Integer> primos = new ArrayList<Integer>();
		boolean isPrimo = false;
		for (int i = 1; i <= 100; i++) {
			for (int j = 1; j <= i; j++) {
				isPrimo = true;
				if ((i % j == 0) && ((i != j) && (j != 1))) {
					isPrimo = false;
					break;
				}				
			}
			if (isPrimo) {
				primos.add(i);
			}
		}
		for (Integer primo : primos) {
			System.out.println(primo);
		}
		
	}
}

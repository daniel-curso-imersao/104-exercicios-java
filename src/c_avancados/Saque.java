package c_avancados;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import classes.Conta;
import util.Util;

/**
 * Crie um programa que faça um saque no arquivo contas.csv. O programa
 * deve receber um id e o valor do saque. Ele deve informar caso o id
 * não seja encontrado ou caso o valor não esteja disponível.
 * 
 * Após o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	public static void main(String[] args) {
		int idEscolhido = 0;
		double saqueSolicitado = 0;
		boolean idEncontrado = false;
		boolean saldoInsuficiente = false;
		Path path = Paths.get("src/contas.csv");
		List<Conta> listaContas = new ArrayList<Conta>();
		List<String> linhas = new ArrayList<String>();
		
		try {
			linhas = Files.readAllLines(path);
			for (String linha : linhas) {
				if (!Util.isPrimeiroItem(linhas, linha)) {
					String[] linhaSplit = linha.split(",");
					int id = Integer.parseInt(linhaSplit[0]);
					String firstName = linhaSplit[1];
					String lastName = linhaSplit[2];
					String email = linhaSplit[3];
					double funds = Double.parseDouble(linhaSplit[4]);															
					Conta conta = new Conta(id, firstName, lastName, email, funds);
					listaContas.add(conta);
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
						
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("Insira um id: ");
			idEscolhido = scanner.nextInt();
			System.out.println("Insira o valor do saque: ");
			saqueSolicitado = scanner.nextDouble();
			
			for (Conta conta : listaContas) {
				if (conta.getId() == idEscolhido) {
					idEncontrado = true;
					if (saqueSolicitado <= conta.getFunds()) {
						double saldoAtualizado = (conta.getFunds() - saqueSolicitado);
						conta.setFunds(saldoAtualizado);
						saldoInsuficiente = false;
					} else {
						saldoInsuficiente = true;
					}
				}
			}
			
			if (!idEncontrado) {
				System.out.println("Id não foi encontrado.");
				return;
			} 
			if (saldoInsuficiente) {
				System.out.println("Saldo insuficiente.");
				return;
			}
			
			StringBuilder sb = new StringBuilder();
			sb.append("id,first_name,last_name,email,funds" + Util.separador);
			listaContas.forEach(conta -> {
				String linhaGravar = MessageFormat.format("{0},{1},{2},{3},{4}", 
						String.valueOf(conta.getId()), conta.getFirstName(), conta.getLastName(),
						conta.getEmail(),  String.valueOf(conta.getFunds()));
				
				if (!Util.isUltimoDaLista(listaContas, conta)) {										
					sb.append(linhaGravar + Util.separador);
				} else {
					sb.append(linhaGravar);
				}
			});
						
			try {
				Util.inicializarArquivoExercicio(path, sb.toString().getBytes());
				System.out.println("Arquivo gravado com sucesso.");
			} catch (IOException e) {			
				System.out.println(e.getMessage());
			}
		}
	}
}

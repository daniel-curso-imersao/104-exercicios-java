package c_avancados;

import java.util.ArrayList;
import java.util.List;

/**
 * Crie um programa que realiza o sorteio de 20 números de 1 à 50, de forma que
 * um número sorteado não pode ser repetido.
 * 
 * Após o sorteio, o progama deve conferir uma cartela pré-determinada e
 * determinar o resultado dentre as possíveis opções:
 * 
 * - Bingo: cartela cheia - Linha: linha cheia (vertical ou horizontal) - Erro:
 * nenhuma das anteriores
 *
 * O programa deve imprimir os números sorteados e o resultado.
 */
public class Bingo {

	public static void main(String args[]) {
		int[][] cartela = { { 10, 21, 34, 43 }, { 7, 15, 11, 50 }, { 41, 9, 33, 2 }, { 1, 2, 34, 49 } };
		int totalNumeros = 50;
		int totalNumerosSorteados = 20;
		int cont = cartela.length;
		int horizontais = 0;
		int verticais = 0;
		boolean isBingo = false;

		List<Integer> sorteados = new ArrayList<Integer>();

		for (int i = 1; i <= totalNumerosSorteados; i++) {
			int numeroSorteado = (int) Math.round(Math.random() * totalNumeros);
			sorteados.add(numeroSorteado);
		}

		// Verificar Horizontal
		for (int i = 0; i < cartela.length; i++) {
			cont = cartela.length;
			for (int j = 0; j < cartela.length; j++) {
				if (sorteados.contains(cartela[i][j])) {
					cont--;
					if (cont == 0) {
						horizontais++;
					}
				}
			}
		}

		// Verificar Vertical
		for (int i = 0; i < cartela.length; i++) {
			cont = cartela.length;
			for (int j = 0; j < cartela.length; j++) {
				if (sorteados.contains(cartela[j][i])) {
					cont--;
					if (cont == 0) {
						verticais++;
					}
				}
			}
		}
		
		// Verificar Bingo
		if ((horizontais == cartela.length) && (verticais == cartela.length)) {
			isBingo = true;
		}

		System.out.println("Números sorteados");
		System.out.println("-----------------------------");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sorteados.size(); i++) {
			if (i == 9) {
				sb.append(sorteados.get(i) + "  ");
				System.out.println(sb.toString());
				sb = new StringBuilder();
			} else {
				sb.append(sorteados.get(i) + "  ");
			}
		}

		System.out.println(sb.toString());

		// Resultado
		System.out.println("Resultado");
		System.out.println("-----------------------------");
		if (isBingo) {
			System.out.println("Bingo!");
		} else if (horizontais > 0) {
			System.out.println("Encheu pelo menos uma horizontal!");
		} else if (verticais > 0) {
			System.out.println("Encheu pelo menos uma vertical!");
		} else {
			System.out.println("Erro!");
		}

	}
}
